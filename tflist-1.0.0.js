function TfList(dom,setting){
    var _this=this;
    this.DOM=dom;
    this.Setting=setting;
    this.module=$(this.DOM+" .tfl-page-module").eq(0);
    this.module.hide();
    this.context=$(this.DOM+" .tfl-context").eq(0);
    $(window).scroll(function(e){_this.scroll(e,_this)});
}
TfList.prototype.Setting={};
TfList.prototype.DOM="";
TfList.prototype.context;
TfList.prototype.module="";
TfList.prototype.list="";
TfList.prototype.pages_height=[];
TfList.prototype.pages_top=[];
TfList.prototype.cache_array=[];
TfList.prototype.curpage=0;
TfList.prototype.hasloaded_page=-1;
TfList.prototype.scrollY=0;
TfList.prototype.getData=function(data_array){
    this.hasloaded_page++;
    this.cache_array.push(data_array);
    this.organizeDisplayArray();
    this.displayList();
}
TfList.prototype.addData=function(data_array){
    this.hasloaded_page++;
    this.cache_array.push(data_array);
    this.displayAddPage();
}
TfList.prototype.displayPage=function(data_array,in_data=null){
    var tmp_list="";
    for(var i in data_array){
        var tmp_module=this.module.html();
        for(var j in data_array[i]){
            tmp_module=tmp_module.replace(new RegExp('%'+j, "g"),data_array[i][j]);
        }
        tmp_list+=tmp_module;
    }
    if(in_data){
        return tmp_list;
    }
    return "<div class='tf-page'>"+tmp_list+"</div>";
}
TfList.prototype.displayList=function(){
    this.context.html("");
    this.pages_height=[];
    this.pages_top=[];
    for(var i in this.cache_array){
        this.list="";
        this.list+=this.displayPage(this.cache_array[i]);
        this.context.append(this.list);
        this.context.find(".tf-page").eq(i).attr("tf-page",i);
        this.pages_height.push(this.context.find(".tf-page").eq(i).height());
        this.pages_top.push(this.getPageTop(i));
    }
}
TfList.prototype.displayAddPage=function(){
    var i=this.cache_array.length-1;
    var page=this.displayPage(this.cache_array[i]);
    this.context.append(page);
    this.context.find(".tf-page").eq(i).attr("tf-page",i);
    this.pages_height.push(this.context.find(".tf-page").eq(i).height());
    this.pages_top.push(this.getPageTop(i));
}
TfList.prototype.scroll=function(e,_this){
    if(_this.curpage!=_this.getCurPage()){
        _this.curpage=_this.getCurPage();
        _this.context.find(".tf-page").eq(_this.getCurPage()-1).css("height",_this.pages_height[_this.getCurPage()-1]+"px");
        _this.context.find(".tf-page").eq(_this.getCurPage()-1).html("");
        _this.context.find(".tf-page").eq(_this.getCurPage()).html(_this.displayPage(_this.cache_array[_this.getCurPage()],true));
    }
    if($("body").scrollTop() == ($("body").height()-$(window).height())){
        console.log("到达底部");
        console.log(_this.Setting.when_scroll_to_bottom);
        if(_this.Setting.when_scroll_to_bottom){
            _this.Setting.when_scroll_to_bottom();
        }
    }
}
TfList.prototype.getPageTop=function(page){
    return this.context.find(".tf-page").eq(page).offset().top
}
TfList.prototype.getCurPage=function(){
    for(var i=0;i<this.pages_top.length;i++){
        if($("body").scrollTop()<this.pages_top[i]){
            return i-1;
        }
    }
    return this.pages_top.length-1;
}
